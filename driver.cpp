#include <iostream>
#include <fstream>
#include <vector>
#include "opcodetab.h"
#include "opcode_error_exception.h"

using namespace std;

int main (int argc, char* argv[]) {

	std::cout << "Hello Team California" << endl;

	opcodetab opcode_table;

	try {

		cout << "Value: ADD, Hex: " << opcode_table.get_machine_code(" ") << ", Size: " << opcode_table.get_instruction_size(" ") << endl;
		cout << "Value: +ADDF, Hex: " << opcode_table.get_machine_code("+ADDF") << ", Size: " << opcode_table.get_instruction_size("+ADDF") << endl;
		cout << "Value: AdDr, Hex: " << opcode_table.get_machine_code("AdDr") << ", Size: " << opcode_table.get_instruction_size("AdDr") << endl;
		cout << "Value: +AnD, Hex: " << opcode_table.get_machine_code("+AnD") << ", Size: " << opcode_table.get_instruction_size("+AnD") << endl;
		//cout << opcode_table.get_machine_code("NOTHING") << endl;
		cout << "Value: +AdDr, Hex: " << opcode_table.get_machine_code("+AdDr") << ", Size: " << opcode_table.get_instruction_size("+AdDr") << endl;
	}

	catch (opcode_error_exception e) {

		cout << e.getMessage() << endl;

	}

	return 0;

}
