/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog2
    CS530, Spring 2016
*/

#ifndef OPCODETAB_H
#define OPCODETAB_H

#include <algorithm>
#include <string>
#include <map>

using namespace std;

class opcodetab {
    public:
        // ctor
        // creates a new dictionary structure and loads all of the opcodes for 
        // the SIC/XE architecture into the table.  Use the STL
        // map for this.
        opcodetab(); 
        
        // takes a SIC/XE opcode and returns the machine code 
        // equivalent as a two byte string in hexadecimal.
        // Example:  get_machine_code("ADD") returns the value 18
        // Note that opcodes may be prepended with a '+'.
        // throws an opcode_error_exception if the opcode is not 
        // found in the table.
        string get_machine_code(string);  
        
        // takes a SIC/XE opcode and returns the number of bytes 
        // needed to encode the instruction, which is an int in
        // the range 1..4.
        // NOTE: the opcode must be prepended with a '+' for format 4.
        // throws an opcode_error_exception if the opcode is not 
        // found in the table.        
        int get_instruction_size(string);
                        
    private:

	// opcode map data structure, contains a string for the
	// opcode hex machine code and an int for the opcode size
	struct opcode_data {

		string machine_code;
		int instruction_size;
		opcode_data(string mc, int is) {

			machine_code = mc;
			instruction_size = is;

		}

	};

	// private opcode map containing a string for the key
	// with the value as the opcode_data struct
	map<string,opcode_data> opcode_map;
	map<string,opcode_data>::iterator iter;

	// initiialize map data structure
	// calls add_to_map for each opcode
	void init_map();
	void add_to_map(string, string,  int);

	// takes in opcode string and checks opcode_map
	// returns true if opcode found
	bool opcode_exists (string);

	// takes in opcode string and finds it in the opcode map
	// checks the instruction size and compares to three
	// return true if opcode is format three
	bool opcode_format_three (string); 
	
	// takes in opcode string and checks to see if a + exists
	bool plus_exists (string);

	// takes in opcode string and compares with opcode map
	map<string,opcodetab::opcode_data>::iterator opcode_lookup (string);

	// takes an opcode string and then calls plus_exists
	// if plus exists, the first character is removed
	string remove_plus_sign (string);

	// takes in opcode string and transforms all to uppercase
	// required so that the user input is not case sensitive
	string uppercase_opcode (string);
	
	// raw user input with no modification of the string
	// required to show user their input when an exception is thrown
	string user_input;

};

#endif  
