/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog2
    CS530, Spring 2016
*/

#include "opcodetab.h"
#include "opcode_error_exception.h"

using namespace std;

opcodetab::opcodetab() {

	init_map();

}

string opcodetab::get_machine_code(string s) {

	iter = opcode_lookup(s);

	opcode_data opcode = iter->second;
	string machine_code = opcode.machine_code;

	return machine_code;

}

int opcodetab::get_instruction_size(string s) {

	iter = opcode_lookup(s);

	opcode_data opcode = iter->second;
	int instruction_size = opcode.instruction_size;

	if(plus_exists(s) && instruction_size == 3) instruction_size = 4;

	return instruction_size;

}

void opcodetab::init_map() {

	string opcode_key[] = { 
				"ADD", "ADDF", "ADDR", "AND", "CLEAR", "COMP", "COMPF", "COMPR", "DIV", "DIVF",
				"DIVR", "FIX", "FLOAT", "HIO", "J", "JEQ", "JGT", "JLT", "JSUB", "LDA",
				"LDB", "LDCH", "LDF", "LDL", "LDS", "LDT", "LDX", "LPS", "MUL", "MULF",
				"MULR", "NORM", "OR", "RD", "RMO", "RSUB", "SHIFTL", "SHIFTR", "SIO", "SSK",
				"STA", "STB", "STCH", "STF", "STI", "STL", "STS", "STSW", "STT", "STX",
				"SUB", "SUBF", "SUBR", "SVC", "TD", "TIO", "TIX", "TIXR", "WD"
			      	};

	int opcode_format[] = {
				3, 3, 2, 3, 2, 3, 3, 2, 3, 3,
				2, 1, 1, 1, 3, 3, 3, 3, 3, 3,
				3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
				2, 1, 3, 3, 2, 3, 2, 2, 1, 3,
				3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
				3, 3, 2, 2, 3, 1, 3, 2, 3
				};

	string opcode_hex[] = {
				"18", "58", "90", "40", "B4", "28", "88", "A0", "24", "64",
				"9C", "C4", "C0", "F4", "3C", "30", "34", "38", "48", "00",
				"68", "50", "70", "08", "6C", "74", "04", "D0", "20", "60",
				"98", "C8", "44", "D8", "AC", "4C", "A4", "A8", "F0", "EC",
				"0C", "78", "54", "80", "D4", "14", "7C", "E8", "84", "10",
				"1C", "5C", "94", "B0", "E0", "F8", "2C", "B8", "DC"
				};

	for( unsigned int i = 0; i < (sizeof(opcode_key)/sizeof(*opcode_key)) ; i++ ) {

		add_to_map(opcode_key[i], opcode_hex[i], opcode_format[i]);

	}

}

void opcodetab::add_to_map(string opcode_name, string opcode_hex, int opcode_size) {

	opcode_map.insert(pair <string,opcode_data> (opcode_name, opcode_data(opcode_hex,opcode_size)));

}

bool opcodetab::opcode_exists(string opcode) {

	if(opcode_map.find(opcode) == opcode_map.end())
		return false;
	return true;

}

map<string,opcodetab::opcode_data>::iterator opcodetab::opcode_lookup(string opcode) {

	user_input = opcode;
	opcode = remove_plus_sign(opcode);
	opcode = uppercase_opcode(opcode);
	
	if(!opcode_exists(opcode)) throw (opcode_error_exception("Error: Opcode " + user_input +" not found"));
	if(!opcode_format_three(opcode) && plus_exists(user_input)) throw (opcode_error_exception("Error: Opcode " + opcode + " contains + but is not format 3"));
	

	return opcode_map.find(opcode);
}

string opcodetab::uppercase_opcode(string opcode) {

	transform(opcode.begin(), opcode.end(), opcode.begin(), ::toupper);
	
	return opcode;

}

bool opcodetab::plus_exists (string opcode) {
	
	if(opcode.empty()) return false;	
	if(opcode.at(0) == '+') return true; 
	else return false;

}

bool opcodetab::opcode_format_three(string opcode) {

	opcode_data the_opcode_data = opcode_map.find(opcode)->second;

	return ( the_opcode_data.instruction_size == 3 );

}
	
	

string opcodetab::remove_plus_sign(string opcode) {

	if(plus_exists(opcode)) {

		opcode.erase(0,1);
		return opcode;

	} else {
		
		return opcode;
		
	}

}

