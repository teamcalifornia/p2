/*  Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif
    masc0882
    Team California
    prog2
    CS530, Spring 2016
*/

#ifndef OPCODE_ERROR_EXCEPTION_H
#define OPCODE_ERROR_EXCEPTION_H

#include <iostream>

using namespace std;

class opcode_error_exception {

	public:
		opcode_error_exception(string s) {
			message = s;
		}
			
		string getMessage() {
			return message;
		}

	private:
		string message;	

};

#endif
