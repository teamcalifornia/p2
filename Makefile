# Mark Mandocdoc, Malik McElroy, Xiaojia Song, Mohamed Sharif 
# masc0882 
# Team California
# prog2
# CS530, Spring 2016

TORM = opcodetab.o driver.o driver
CC = g++
CCFLAGS = -g -O3 -Wall -Wpointer-arith -Wcast-qual -Wwrite-strings

driver: driver.o opcodetab.o
	${CC} ${CCFLAGS} -o driver driver.o opcodetab.o

driver.o: driver.cpp
	${CC} ${CCFLAGS} -c driver.cpp

opcodetab.o: opcodetab.cc opcodetab.h opcode_error_exception.h
	${CC} ${CCFLAGS} -c opcodetab.cc

clean:
	rm -f ${TORM}

